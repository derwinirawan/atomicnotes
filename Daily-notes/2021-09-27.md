## NALARASA
	- Hari ini saya diundang untuk berbicara oleh pengelola podcast NALARASA. Pada kesempatan itu, saya diminta menyampaikan isi dari artikel saya yang berjudul **we need a new set of measure to assess research in earth science**.
	- Dalam artikel tersebut saya menjelaskan tentang [[pages/Acara sains terbuka Agt-Okt 2021]]. Salah satu yang saya bicarakan adalah aspek penilaian riset yang saat ini sangat kental dengan nuansa [[prestige and impact]].
	- Mbak Diana adalah pihak yang menggagas podcast ini.
	- Saya menjelaskan bahwa salah satu yang penting dalam sebuah penelitian adalah apakah hasilnya sampai kepada kalangan luas. Terdengar klise tapi pada dasarnya tidak ada yang dirugikan ketika Anda memastikan hal itu terjadi.
	- Read also:
		- [[Scicomm in the first hand]]
		- [[open access meaning]]
		
		-
-