---
tags: #ideas, #ideation, #researchideas, #designthinking
---
# basics
- creating ideas
- developing new ideas to solve a problem
- abstract -> not exist change it to real thing
- don't forget to set restrictions or [[limitations]]

- getting as many as ideas at the beginning
- putting them all on the table
- https://youtu.be/zbLxs6te5to
- <iframe width="560" height="315" src="https://www.youtube.com/embed/zbLxs6te5to" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- first [[novelty over relevance]]
- find the middle ground
- be aware of trade offs [[pages/ide, kreativitas, inovasi]]
	- more time to learn new skills?
	- more time to learn new classes?
	- willingness to go all wet 

# steps
- to find
- to research
- to create
- to test
- to revise
- to deliver
	- begins with a prototype (physical mock up)
		- **explore**: is it working? can we think of something else?
		- **inspire**
		- **converse**: does it work? feedback?
		- beware of **failures**


# don't be afraid to branch out
- select another methods
- select another subject
- etc -> lots of #whatifs





