# kelembagaan -> berkaitan dengan:
- taksonomi, nomenklatur
- kewenangan
- sumber daya
- operasional
- kelembagaan pernah kuat, tapi berdampak seolah F/S dan PS tidak punya posisi tawar
- kelembagaan tidak kuat, dampaknya TPB tidak punya posisi tawar atau posisi untuk mengatur
# kondisi mahasiswa
- kasus gangguan kesehatan mental		
	- banyak kasus di RS Melinda
- jumlah besar vs sumber daya terbatas
	- terkait dengan kelembagaan dan kewenangan
	- kewenangan sempit -> dana kecil
	- kewenangan luas -> dana besar
	- distribusi dana di TPB dan di Fak/Sek
# arus pendanaan
- program ada di TPB tapi dana ada yang di fak/sek
- TPB menjadi lemah kewenangannya
