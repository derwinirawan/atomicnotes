# apa yang terjadi
- tags: #tobamaninjau, #africanlakes 

- peningkatan kadar sulfur/belerang secara tiba-tiba dan masif
- mungkin ada gas lain juga yang terlibat
- mirip proses penimbunan gas CH4 metana di [[Leuwigajah]]

# bagaimana prosesnya
- kantung-kantung sulfur?
	- 
- dari mana?
	- permukaan
		- 
	- bawah permukaan
		- volkanisme


# contoh di tempat lain
## One Day In 1986, Thousands Of People And Animals Around Lake Nyos Were Found Dead
- link https://www.iflscience.com/health-and-medicine/one-day-in-1986-thousands-of-people-and-animals-around-lake-nyos-were-found-dead/
	- > There was no sign of any struggle here or in the village below, where hundreds of others lay dead – roughly where they would have been at around 9 pm when that ominous sound had occurred. The only other clues were the smell of rotten eggs, and strange marks on the bodies of the dead and the living.
- More on Lake Nyos https://en.wikipedia.org/wiki/Lake_Nyos



## Suspicious Deaths of 58 Cows In North Dakota
- link https://www.agweb.com/news/livestock/beef/suspicious-deaths-58-cows-north-dakota


## Another mass fish kill hits Indonesia’s largest lake
- link https://news.mongabay.com/2018/08/another-mass-fish-kill-hits-indonesias-largest-lake/
	- > It was the second mass fish kill in as many years in the lake, which is located in North Sumatra. In 2016, millions of fish also [turned up dead](https://news.mongabay.com/2016/08/why-did-millions-of-fish-turn-up-dead-in-indonesias-giant-lake-toba/). Researchers attributed that incident to a sudden depletion of oxygen in the water, the result of a buildup of pollutants in the lake, unfavorable weather conditions and unsustainable practices by local aquafarmers.


## Why did millions of fish turn up dead in Indonesia’s giant Lake Toba?
- link https://news.mongabay.com/2016/08/why-did-millions-of-fish-turn-up-dead-in-indonesias-giant-lake-toba/
	- > In 2014, Bogor Agricultural University researchers [classified the lake as eutrophic](https://www.was.org/meetings/mobile/MG_Paper.aspx?i=43140), meaning it had become excessively rich in nutrients like nitrogen and phosphorus — and prone to oxygen-devouring algal blooms. The Environment and Forestry Ministry’s inquiry into the May fish kill found that the lake’s [phosphorus content had tripled](http://www.mongabay.co.id/2016/06/20/makin-kritis-restorasi-danau-toba-jadi-prioritas-seperti-apa/) since 2012.-> [[eutrophic]]
	- > “Aquaculture shouldn’t be solely blamed for lake pollution,” Josh Oakley, an environmental specialist who studied the lake’s carrying capacity for aquaculture production, told Mongabay. Pollutants come from a variety of sources, such as agricultural runoff and sewage from houses and hotels.

## Fish Kills
- link https://www.epchc.org/home/showpublisheddocument/546/636415092650330000#:~:text=Severe%20fish%20kills%20occur%20when,combine%20to%20cause%20fish%20kills.
	- > Most times, fish can tolerate temporarily depressed DO levels (hypoxia). Severe fish kills occur when several contributory factors occur simultaneously. Prolonged cloudy weather, drought conditions, overcrowded fish populations, excessive algal or other plant growths, and high water temperatures are the most common factors that combine to cause fish kills. In Florida, most DO-related fish kills occur in the warmer months from May through September, although winter cold fronts can also trigger DO lags. A typical scenario occurs when fish are observed at the water surface appearing to gasp for breath. It is not uncommon for many of the larger fish to die, since they need more oxygen, while the smaller fish seem to be unaffected.