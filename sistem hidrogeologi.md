- tags: #hidrogeologi, #cekunganbandung

# Sistem hidrogeologi

## Sistem input
- Komponen input untuk sebuah sistem hidrogeologi tidak hanya air hujan.
- Air permukaan (sungai, danau, laut) juga dapat menjadi inputnya [[sistem input hidrogeologi]]
- 

## Sistem proses
- Proses yang terjadi bisa apa saja, tapi intinya akan berkaitan dengan interaksi antara air, batuan, dan udara yang ada di dalam akuifer.
- Peresapan, zona tidak jenuh, jenis tanah, jenis batuan [[sistem proses hidrogeologi]]
- Dalam hal ini proses interaksi ini akan berbeda antara [[zona tidak jenuh]] dengan [[zona jenuh]].

## Sistem output
- Komponen yang terlihat di sisi output adalah mata air dan sumur. Mata air di sini bisa saja tidak terlihat tapi langsung berinteraksi dengan air sungai, air danau, atau air laut ([[sub marine groundwater discharge]]).
- Interaksi air tanah dengan akuifer akan terlihat di sini. [[sistem output hidrogeologi]]
- 




# Video

## Tentang Cekungan Bandung

Dalam video ini saya menjelaskan tentang Cekungan Bandung-Soreang dalam konteksnya dalam geologi urban. Namun demikian dalam penjelasan saya ada beberapa poin penting tentang proses input, proses, dan output dalam sistem hidrogeologinya.

<iframe width="560" height="315" src="https://www.youtube.com/embed/OJhetv_rbgQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Tentang hidrogeologi di sekitar kita

Di sini saya menjelaskan sistem hidrogeologi secara ringkas, serta berbagai sistem input-outputnya. Dalam penjelasan saya ada sisipan beberapa contoh sederhana. Video ini merupakan rekaman webinar untuk guru-guru SMA. Simak juga berbagai pertanyaannya.

<iframe width="560" height="315" src="https://www.youtube.com/embed/LhD2MuKnIMU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
