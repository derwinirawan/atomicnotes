---
tags: #seanos2021, #openscience, #scicomm, #trustworthyscience, #publictrust 
---

# Interview with Prof. Simine Varzine

> science: it works whether you believe in it or not

- crises in trust in science
	- replication crises
	- case study: psychology, social sciences, biological sciences, medical sciences
	- [[pages/reproduksi dan replikasi]]

## how we evaluate research

- previously: [[prestige and impact]]
- 