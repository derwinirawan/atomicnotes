---
tags: #wp4, #deltares
---

- World Bank Project supervised by Bappenas
	- identify [[the components of floods]]
		- root causes 
		- drivers
		- pressure
		- impacts


- Four domain of programs:
	- resilient [[coastal buffer]]
	- [[upstream restoration]] of retention and absorption capacities
	- [[urban river corridor improvement]]
	- [[urban drainage system]]


- [[social interactions in flood hazards]]
- [[soil erosions]]
- difficult if connected to institution
	- dinas sosial -> urban [[slump area]]
	- inter-institution agreement is hard -> [[co-creative process]]
- why [[WP4-Bima]], [[WP4-Manado]], [[WP4-Pontianak]]
	- from previous project
	- have quantitative analysis
