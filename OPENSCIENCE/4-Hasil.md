[[1-Abstrak]]
[[2-Pendahuluan]]
[[3-Metode]]

[[5-Diskusi]]
[[6-Ucapan terima kasih]]
[[7-Pendanaan]]
[[8-Konflik kepentingan dan kontribusi]]
[[9-Daftar pustaka]]
[[10-Material pendukung]]

## Jumlah total dokumen dan distribusinya

Di tingkat internasional, penelusuran kami menemukan (Tabel 1) hampir 4,5 juta dokumen ilmu bumi dari Scopus dan WoS, lebih dari 4,6 juta dari GS, hampir 2 juta dari Dimensions, dan hampir setengah juta dari Lens. Di tingkat nasional atau regional, jurnal yang diindeks SciELO mengambil lebih dari 46.000 dokumen, hanya di bawah setengah total dari database Prancis HAL (lebih dari 95.000 di hal.archives-ouvertes.fr/browse/domain).  Indeks Kutipan Sains Rusia menemukan 39.581 dokumen; Indeks Kutipan Korea (KCI) diperoleh 17.156; dan Garuda (pengindeks nasional Indonesia) diperoleh 4.027.

Jumlah artikel yang diterbitkan dalam bahasa Inggris (gabungan artikel OA dan paywalled) yang diambil dari database internasional (Scopus dan WoS) - total lebih dari 1 juta dokumen - jauh melebihi jumlah yang diambil dari database nasional atau regional: KCI, RSCI , dan SciELO bersama-sama hanya menghasilkan sekitar 25.000 dokumen dalam bahasa Inggris, menunjukkan bahwa dokumen dalam bahasa Korea dan Rusia serta dalam bahasa daerah lainnya mendominasi ketiga database tersebut (Tabel 1).
Proporsi makalah OA terhadap total bervariasi menurut platform: proporsi dokumen ilmu bumi OA di database nasional dan regional (Garuda dan SciELO) (Mean = 100%) jauh lebih besar daripada di database lain (Mean = 24 %) (Tabel 1). Di antara database internasional, Dimensions (32,7%) dan Lens (31,7%) memiliki proporsi artikel OA yang lebih besar daripada Scopus (26,4%) (Tabel 1).

Seiring waktu, jumlah dokumen OA dan ilmu bumi berbayar terus meningkat, meskipun sangat lambat, sepanjang 2010–2019 di semua database (Gambar 1, diekstraksi pada 23 Maret 2020; tautan ke plot dan kumpulan data: https://datawrapper.dwcdn.net/CgpLO/1/) dan SciELO mencatat peningkatan tajam sejak 2019.

![[ese-tab-1.png]]

![[ese-fig-1.png]]

## Cakupan geografis database atau layanan pengindeksan

Kami mengurutkan jumlah total dokumen ilmu bumi dari masing-masing dari empat database (Lens, Dimensi, SciELO, dan Scopus; Tabel 2) berdasarkan negara menggunakan afiliasi penulis dan rekan penulis dan kemudian memberi peringkat negara-negara dalam urutan menurun dari total output untuk memastikan sembilan negara teratas di setiap database. Cakupan geografis bervariasi dengan database: misalnya, AS menduduki peringkat pertama di Scopus dan Lens, tetapi tidak di Dimensions dan SciELO; Cina ditampilkan di semua database; dan Rusia hanya ditampilkan di Scopus dan Lens.

![[ese-tab-2.png]]

## Peraturan tentang kenaikan pangkat peraturan kenaikan pangkat dosen

Kami menetapkan bobot atau skor untuk setiap makalah sebagai berikut: 40 (skor tertinggi) jika diterbitkan dalam jurnal internasional dengan faktor dampak di atas 0,05; 30 jika faktor dampak jurnal internasional adalah 0,05 atau lebih rendah atau jika makalah diterbitkan sebagai bagian dari volume prosiding yang terindeks di Scopus; 25 jika diterbitkan dalam jurnal nasional berperingkat 1 atau 2 oleh SINTA (Indeks Iptek Indonesia); dan 10 (nilai terendah) jika diterbitkan pada jurnal nasional yang tidak terindeks SINTA.

Sistem penilaian ini sesuai dengan peraturan lain yang mendefinisikan artikel sebagai 'internasional' jika ditulis dalam bahasa Inggris atau bahasa Perserikatan Bangsa-Bangsa lainnya, ditinjau oleh peninjau internasional, dan diterbitkan dalam jurnal internasional. Kriteria ini secara otomatis mengecualikan semua artikel yang ditulis dalam Bahasa Indonesia, ditinjau oleh reviewer lokal, dan diterbitkan di jurnal lokal agar tidak dianggap sebagai artikel bereputasi baik — sebuah rintangan utama bagi peneliti Indonesia yang ingin mempublikasikan temuan penelitian mereka dalam bahasa ibu mereka.

[[1-Abstrak]]
[[2-Pendahuluan]]
[[3-Metode]]

[[5-Diskusi]]
[[6-Ucapan terima kasih]]
[[7-Pendanaan]]
[[8-Konflik kepentingan dan kontribusi]]
[[9-Daftar pustaka]]
[[10-Material pendukung]]

