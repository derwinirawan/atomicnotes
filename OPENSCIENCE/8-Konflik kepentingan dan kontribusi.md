[[1-Abstrak]]
[[2-Pendahuluan]]
[[3-Metode]]
[[4-Hasil]]
[[5-Diskusi]]
[[6-Ucapan terima kasih]]
[[7-Pendanaan]]

[[9-Daftar pustaka]]
[[10-Material pendukung]]

## Konflik kepentingan

Para penulis menyatakan bahwa mereka tidak memiliki kepentingan yang bersaing.

## Pernyataan kontribusi

- DEI: pencetus ide awal, penyusunan konsep, pengambilan data, analisis, penulisan manuskrip
- JA: penyusunan konsep, pengambilan data, analisis, penulisan manuskrip
- JPT: penyusunan konsep, pengambilan data, analisis, penulisan manuskrip
- OP: penyusunan konsep, pengambilan data, analisis, penulisan manuskrip