[[1-Abstrak]]
[[2-Pendahuluan]]
[[3-Metode]]
[[4-Hasil]]
[[5-Diskusi]]

[[7-Pendanaan]]
[[8-Konflik kepentingan dan kontribusi]]
[[9-Daftar pustaka]]
[[10-Material pendukung]]

Kami ingin mengucapkan terima kasih kepada komunitas komunitas Indonesia Open Science atas umpan balik pada draf pertama dan pengulas anonim yang meluangkan waktu mereka yang berharga untuk mengevaluasi draf dan berbagi wawasan mereka. Kami juga ingin menyebutkan beberapa orang yang telah membantu kami merumuskan masalah secara sistematis dalam publikasi ilmiah dan penelitian dan pengembangan di Indonesia: Surya Darma Hamonangan Dalimunthe dari Universitas Islam Negeri Sumatera Utara, Dr Hendro Subagyo dan dedikasi tim dari Lembaga Ilmu Pengetahuan Indonesia (LIPI) untuk menjadi tuan rumah RINarxiv, Stefaniia Ivashchenko dari Open Science TV dan Björn Brembs dari Universität Regensburg untuk kampanye kuat mereka melawan komersialisasi hasil akademik, dan Peter Murray-Rust dari University of Cambridge untuk menunjuk pentingnya text mining.