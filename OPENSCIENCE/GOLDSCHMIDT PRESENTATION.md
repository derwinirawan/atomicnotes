Earth sciences is one of those sensitive field sciences that are closely needed to solve local problems within local physical and social settings. Earth researchers find state-of-the-art of topics in earth sciences by using scientific databases, conduct research on the topics, and write about them. However, the accessibility, readability, and usability of those articles for local communities are major problems in measuring the impact of research, although it may be covered by well-known international scientific databases.


To ascertain empirically whether there are differences in document distribution, in the proportions of openly accessible documents, and in the geographical coverage of earth sciences topics as revealed through analyses of documents retrieved from scientific databases and to propose new measures for assessing the impact of research in earth sciences based on those differences.


Relevant documents were retrieved using ‘earth sciences’ as a search term in English and other languages from ten databases of scientific publications. The results of these searches were analysed using frequency analysis and a quantitative- descriptive design.


1. The number of articles in English from international databases exceeded the number of articles in native languages from national-level databases. 
2. The number of open-access (OA) articles in the national databases was higher than that in other databases. 
3. The geographical coverage of earth science papers was uneven between countries when the number of documents retrieved from closed-access commercial databases was compared to that from the other databases. 
4. The regulations in Indonesia related to promotion of lecturers assign greater weighting to publications indexed in Scopus and the Web of Science (WoS) and publications in journals with impact factors are assigned a higher weighting.


The dominance of scientific articles in English as well as the paucity of OA publications indexed in international databases (compared to those in national or regional databases) may have been due to the greater weighting assigned to such publications. Consequently, the relevance of research reported in those publications to local communities has been questioned. This article suggests some open-science practices to transform the current regulations related to promotion into a more responsible measurement of research performance and impact.