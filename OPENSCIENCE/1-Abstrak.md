[[2-Pendahuluan]]
[[3-Metode]]
[[4-Hasil]]
[[5-Diskusi]]
[[6-Ucapan terima kasih]]
[[7-Pendanaan]]
[[8-Konflik kepentingan dan kontribusi]]
[[9-Daftar pustaka]]
[[10-Material pendukung]]

**Latar Belakang:**

-   Ilmu kebumian merupakan salah satu bidang ilmu yang sensitif yang sangat dibutuhkan untuk memecahkan masalah lokal dalam setting fisik dan sosial lokal.
-   Peneliti bumi menemukan topik mutakhir dalam ilmu bumi dengan menggunakan basis data ilmiah, melakukan penelitian tentang topik tersebut, dan menulis tentang topik tersebut.
-   Namun, aksesibilitas, keterbacaan, dan kegunaan artikel-artikel tersebut untuk masyarakat lokal merupakan masalah utama dalam mengukur dampak penelitian, meskipun mungkin tercakup oleh database ilmiah internasional yang terkenal.

**Tujuan:**

-   Untuk memastikan secara empiris apakah ada perbedaan dalam distribusi dokumen, dalam proporsi dokumen yang dapat diakses secara terbuka, dan dalam cakupan geografis topik ilmu kebumian sebagaimana terungkap melalui analisis dokumen yang diambil dari database ilmiah dan
-   untuk mengusulkan langkah-langkah baru untuk menilai dampak dari penelitian ilmu kebumian berdasarkan perbedaan tersebut.

**Metode:** Dokumen yang relevan diambil menggunakan 'ilmu bumi' sebagai istilah pencarian dalam bahasa Inggris dan bahasa lain dari sepuluh database publikasi ilmiah. Hasil penelusuran tersebut dianalisis menggunakan analisis frekuensi dan desain deskriptif kuantitatif.

**Hasil:**

1.  Jumlah artikel dalam bahasa Inggris dari database internasional melebihi jumlah artikel dalam bahasa asli dari database tingkat nasional.
2.  Jumlah artikel open-access (OA) di database nasional lebih banyak daripada di database lain.
3.  Cakupan geografis makalah ilmu bumi tidak merata antar negara ketika jumlah dokumen yang diambil dari database komersial akses tertutup dibandingkan dengan yang dari database lain.
4.  Peraturan di Indonesia terkait promosi dosen memberikan bobot yang lebih besar pada publikasi yang terindeks Scopus dan Web of Science (WoS) dan publikasi pada jurnal dengan impact factor diberi bobot yang lebih tinggi.

**Kesimpulan:**

-   Dominasi artikel ilmiah dalam bahasa Inggris serta kurangnya publikasi OA yang diindeks dalam database internasional (dibandingkan dengan yang ada di database nasional atau regional) mungkin disebabkan oleh bobot yang lebih besar yang diberikan untuk publikasi tersebut.
-   Akibatnya, relevansi penelitian yang dilaporkan dalam publikasi tersebut terhadap masyarakat lokal dipertanyakan.
-   Artikel ini menyarankan beberapa praktik sains terbuka untuk mengubah peraturan saat ini terkait promosi menjadi pengukuran kinerja dan dampak penelitian yang lebih bertanggung jawab.

[[2-Pendahuluan]]
[[3-Metode]]
[[4-Hasil]]
[[5-Diskusi]]
[[6-Ucapan terima kasih]]
[[7-Pendanaan]]
[[8-Konflik kepentingan dan kontribusi]]
[[9-Daftar pustaka]]
[[10-Material pendukung]]