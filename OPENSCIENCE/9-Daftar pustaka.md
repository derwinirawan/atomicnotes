[[1-Abstrak]]
[[2-Pendahuluan]]
[[3-Metode]]
[[4-Hasil]]
[[5-Diskusi]]
[[6-Ucapan terima kasih]]
[[7-Pendanaan]]
[[8-Konflik kepentingan dan kontribusi]]

[[10-Material pendukung]] 

1. Wardhono H, Budiyono B, Hartati FK. Desa wisma siaga bencana di Desa Bungurasih Sidoarjo. Journal Community Development and Society. Dr. Soetomo University; 2020;2(1):56–72. Available from: https://doi.org/10.25139/cds.v2i1.2512
2. Gadd E. Mis-measuring our universities: how global university rankings don’t add up. SocArXiv; 2021 Mar. Available from: https://doi.org/10.31235/osf.io/gxbn5 
3. Komisi Pemberantasan Korupsi. Kajian tata kelola dana penelitian. Direktur Penelitian dan Pengembangan KPK; 28 Dec. 2018. Available from: https://www.kpk.go.id/images/pdf/LHKA-Dana-Penelitian-2018.pdf
4. CokroTV. Nadiem habisi tiga dosa di dunia pendidikan. Youtube; 17 Aug. 2020. Available from: https://www.youtube.com/watch?v=c-tqIgULIlU&feature=youtu.be. Accessed 19 Aug. 2020.
5. Irawan DE. Visualisasi posisi riset beberapa perguruan tinggi di Indonesia. GitHub; 7 Sept. 2020. Available from: https://github.com/dasaptaerwin/pemeringkatanpt2020/blob/master/REKAPITULASI/PPT-VOS-CITATION.pdf
6. Abraham J, Irawan DE, Dalimunthe S. Jalan evolusi bibliometrik Indonesia. The Conversation; 8 Jan. 2019. Available from: https://theconversation.com/jalan-evolusi-bibliometrik-indonesia-104781
7. Peroni S, Shotton D. OpenCitations, an infrastructure organization for open scholarship. Quantitative Science Studies. MIT Press - Journals; 2020;1(1):428–44. Available from: https://doi.org/10.1162/qss_a_00023
8. DORA. To date, 19,612 individuals and organizations in 145 countries have signed DORA. DORA; 17 May 2021. Available from: https://sfdora.org/signers/ 9 Center for Open Science. New measure rates quality of research journals’ policies to promote transparency and reproducibility. COS; 10 Feb. 2020. Available from: https://www.cos.io/about/news/new-measure-rates-quality-research-journals-policies-promote-transparency-and-reproducibility
9. Irawan DE, Abraham J, Zein RA, Ridlo IA, Aribowo EK. Open access in Indonesia. Development and Change. Wiley; 2021;52(3):651–60. Available from: https://onlinelibrary.wiley.com/doi/epdf/10.1111/dech.12637
10. Irawan DE, Abraham J, Tennant J, Pourret O. Global flow of Earth science scientific articles based on several databases. Zenodo; 20 Nov. 2020. Available from: https://zenodo.org/record/4283016
11. Rochmyaningsih D. The developing world needs more than numbers. Nature. Springer Science and Business Media LLC; 2017;542(7639):7-7. Available from: https://doi.org/10.1038/542007a
12. Suryandari S. Belum ada pendataan dampak ekonomi dari hasil riset. Media Indonesia; June 2019. Available from: https://mediaindonesia.com/read/ detail/243326-belum-ada-pendataan-dampak-ekonomi-dari-hasil-riset
13. Moravcsik A. Example of an active citation. Princeton University; June 2014. Available from: https://www.princeton.edu/~amoravcs/library/Stevens-ExampleActiveCitation.pdf
14. Lazarev VS. Manipulation of bibliometric data by editors of scientific journals. European Science Editing. 2019;45(4):92-93. Available from: https://doi. org/10.20316/ESE.2019.45.19011
15. Purnell PJ. The effect of the Indonesian higher education evaluation system on conference proceedings publications. ArXiv; 26 Oct. 2019. Available from: https://arxiv.org/abs/1910.12018
16. Lewis DW. Proposal for a standard article metrics dashboard to replace the Journal Impact Factor. ScholarWorks; July 2019. Available from: https:// scholarworks.iupui.edu/handle/1805/19850
17. Royal Netherlands Academy of Arts and Sciences. Quality indicators for research in the Humanities. Koninklijke Nederlandse Akademie van Wetenschappen; May 2011. Available from: https://www.knaw.nl/shared/resources/actueel/publicaties/pdf/quality-indicators-for-research-in-the-humanities
18. An1mage Jurnal Studi Kultural. About the Journal. 2021. Available from: https://web.archive.org/web/20210224204217/https://journals.an1mage.net/index. php/ajsk
19. Chan Zuckerberg Initiative. Open science. CZI; 2020. Available from: https://chanzuckerberg.com/science/programs-resources/open-science/  
20. Sadikin RA. Penanganan COVID-19, ilmuwan Indonesia merasa tak dilibatkan pemerintah. Suara; 19 May 2020. Available from: https://www.suara.com/news/2020/05/19/130434/penanganan-covid-19-ilmuwan-indonesia-merasa-tak-dilibatkan-pemerintah?page=all
21. Pourret O, Suzuki K, Takahashi Y. Our Study is published, but the journey is not finished! Elements. Mineralogical Society of America; 2020 Aug 1;16(4):229–30. Available from: https://doi.org/10.2138/gselements.16.4.229
22. Nasih M. Inovasi atau terjajah kembali. Jawa Pos; 17 Aug. 2020. Available from: https://www.jawapos.com/opini/17/08/2020/inovasi-atau-terjajah-kembali/ 24 Dinata RYW. Obat Covid-19 Unair tidak diprioritaskan publikasi internasional. AyoSurabaya; 18 Aug. 2020. Available from: https://www.ayosurabaya.com/read/2020/08/18/2653/obat-covid-19-unair-tidak-diprioritaskan-publikasi-internasional
23. Irawan DE, Abraham J. Set them free: A manifesto toward moving beyond academic imperialism. Commonplace; Mar 2021. Available from: https://doi. org/10.21428/6ffd8432.5ee70f55