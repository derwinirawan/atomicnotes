[[1-Abstrak]]
[[2-Pendahuluan]]
[[3-Metode]]
[[4-Hasil]]
[[5-Diskusi]]
[[6-Ucapan terima kasih]]

[[8-Konflik kepentingan dan kontribusi]]
[[9-Daftar pustaka]]
[[10-Material pendukung]]

Telaah ini didukung oleh Program PPMI Institut Teknologi Bandung 2019 dan oleh Kantor Ristek Universitas Bina Nusantara, sebagai bagian dari Hibah Penelitian Internasional Universitas Bina Nusantara (PIB; Kontrak No. 026 / VR.RTT / IV / 2020 6 April 2020) di mana penelitian ini berfokus pada pencegahan korupsi akademik dengan meningkatkan kesadaran akan metode yang lebih tepat untuk mengukur dampak penelitian.