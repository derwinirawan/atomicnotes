[[1-Abstrak]]
[[2-Pendahuluan]]

[[4-Hasil]]
[[5-Diskusi]]
[[6-Ucapan terima kasih]]
[[7-Pendanaan]]
[[8-Konflik kepentingan dan kontribusi]]
[[9-Daftar pustaka]]
[[10-Material pendukung]]

## Basis data

Kumpulan data lengkap yang digunakan untuk semua analisis dalam penelitian ini tersedia secara online.11 Kumpulan data dibangun dengan mencari basis data berikut: Dimensions, Garuda, Google Scholar (GS), Korean Citation Index (KCI), Lens, Russian Science Citation Index (RSCI), Scientific Electronic Library Online (SciELO), Scopus (diambil melalui Institut Teknologi Bandung, Indonesia), dan Web of Science (WoS; diambil melalui UniLaSalle, Prancis) (Tabel Tambahan 1).

## Strategi pencarian

Di setiap basis data, kami mencari dokumen-dokumen yang terkait dengan ilmu kebumian. Karena basis data memiliki filter dan menu pencarian yang berbeda, maka strategi pencarian disesuaikan (Tabel Tambahan 2). Untuk Dimensions, KCI, Lens, RSCI, Scopus, dan WoS, kami menggunakan filter dan bidang ilmu yang tersedia untuk mencari dokumen yang berkaitan dengan ilmu kebumian. Di GS dan Garuda tidak mungkin menyaring dokumen berdasarkan bidang ilmunya; oleh karena itu, kami mengetikkan kata kunci yang relevan. Karena GS mengindeks sebagian besar dokumen online tanpa memandang bahasa dan karena Garuda hanya mengindeks jurnal Indonesia, kami memutuskan untuk menggunakan bahasa lokal sebagai kata kunci di GS dan Garuda untuk mengambil semua dokumen relevan yang diterbitkan dalam bahasa selain bahasa Inggris.

Menggunakan semua basis data, kami mengekstrak item informasi berikut.

- Jumlah total catatan: total dokumen dan dokumen akses terbuka, menurut tahun
- Jenis dokumen: artikel, prosiding konferensi, dan bab buku
- Judul sumber: judul jurnal (tanpa filter)
- Area penelitian: area penelitian ilmu bumi menggunakan filter yang sesuai di basis data
- Negara: negara tempat tinggal semua penulis, tanpa penyaringan apapun
- Sumber pendanaan: tanpa penyaringan apapun
- Bahasa: Inggris dan bahasa selain bahasa Inggris (tergantung ketersediaan filter bahasa)

## Analisis statistik

Kami menggunakan statistik deskriptif untuk menggambarkan karakteristik dokumen di setiap basis data untuk menghasilkan tabel dan kemudian menghasilkan grafik yang sesuai menggunakan [Datawrapper.de](https://datawrapper.de), sebuah platform daring.

[[1-Abstrak]]
[[2-Pendahuluan]]

[[4-Hasil]]
[[5-Diskusi]]
[[6-Ucapan terima kasih]]
[[7-Pendanaan]]
[[8-Konflik kepentingan dan kontribusi]]
[[9-Daftar pustaka]]
[[10-Material pendukung]]