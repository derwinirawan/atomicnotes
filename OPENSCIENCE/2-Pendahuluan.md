[[1-Abstrak]]
[[3-Metode]]
[[4-Hasil]]
[[5-Diskusi]]
[[6-Ucapan terima kasih]]
[[7-Pendanaan]]
[[8-Konflik kepentingan dan kontribusi]]
[[9-Daftar pustaka]]
[[10-Material pendukung]]

Penelitian dalam ilmu kebumian, sebagaimana bidang ilmu lainnya, berupaya untuk memecahkan masalah masyarakat lokal yang berkaitan dengan bumi. Cara tercepat untuk mengkomunikasikan hasil penelitian tersebut adalah dengan menggunakan infrastruktur dan bahasa lokal. Sebagai contoh, di Indonesia, banyak [Program Desa Siaga Bencana](https://ejournal.kemsos.go.id/index.php/Sosioinforma/article/view/69) telah dikembangkan berdasarkan kajian ilmiah menggunakan Bahasa Indonesia1 dan disebarluaskan melalui [INA-Rxiv](https://osf.io/preprints/inarxiv/). Dibandingkan dengan rekan-rekan mereka yang berbahasa Inggris, para ilmuwan bumi dari negara-negara yang kurang terwakili atau yang tidak berbahasa Inggris memikul beban yang lebih berat karena mereka memiliki tanggung jawab ganda: 

1. mereka harus mempublikasikan dalam jurnal yang ditinjau sejawat dan bereputasi menggunakan standar bahasa Inggris yang tinggi untuk memenuhi metrik akademik konvensional seperti jumlah kutipan, indeks-h, faktor dampak jurnal, dan Peringkat Jurnal SCImago — metrik yang didukung oleh peraturan nasional yang relevan untuk mengukur kinerja akademisi untuk promosi `(ket: kenaikan jabatan fungsional)` dan 
2. mereka juga harus ditujukan pada komunitas penjangkauan dan pelibatan menggunakan bahasa lokal untuk melaksanakan tanggung jawab mereka kepada masyarakat (walaupun ini jarang menjadi prioritas mereka mengingat jam kerja mereka yang terbatas).

Sayangnya, dengan secara aktif berpartisipasi dalam permainan peringkat dunia dan berusaha untuk dianggap sebagai universitas kelas dunia, universitas sangat mendorong staf mereka untuk menerbitkan dalam bahasa Inggris, karena kriteria yang dipertimbangkan untuk penilaian dan peringkat biasanya dirancang oleh, dan bermanfaat bagi, yang disebut negara-negara WEIRD (kependekan dari Western, Educated, Industrialized, Rich, and Democracy).[2](https://doi.org/10.31235/osf.io/gxbn5) Di Indonesia, bias ini dapat menyebabkan penerapan hasil penelitian yang tidak memadai untuk memenuhi kebutuhan publik dan, pada akhirnya, membuang-buang uang dan sumber daya dibelanjakan oleh negara untuk penelitian tersebut.[3](https://www.kpk.go.id/images/pdf/LHKA-Dana-Penelitian-2018.pdf) Tanggung jawab penelitian untuk berkontribusi kepada masyarakat juga baru-baru ini ditekankan oleh presiden Indonesia dan Menteri Pendidikan dan Kebudayaan Indonesia dan sekarang ditampilkan dalam rancangan kebijakan - untuk diterapkan dalam waktu dekat - terkait terhadap karir dosen dan kinerja perguruan tinggi.[4](https://www.youtube.com/watch?v=c-tqIgULIlU&feature=youtu.be)

<iframe src="https://www.youtube.com/watch?v=c-tqIgULIlU&feature=youtu.be"></iframe>

Pertanyaan penelitian utama yang dibahas dalam penelitian ini adalah: Apakah kita memerlukan perspektif baru dalam mengukur dampak penelitian? Ukuran konvensional dari dampak tersebut berlaku pada tiga tingkat, yaitu pada tingkat jurnal (faktor dampak atau SCImago Journal Rank, misalnya), pada tingkat artikel (berapa kali artikel dikutip, misalnya ), dan pada tingkat penulis (indeks-h, misalnya). Metrik ini membentuk dasar kriteria yang digunakan secara luas untuk mengukur reputasi institusi serta penulis dan kelompok penelitian.

Orang Indonesia cenderung membaca dan mengutip banyak artikel Indonesia; oleh karena itu, makalah yang ditulis dalam bahasa Inggris mengumpulkan lebih sedikit kutipan.[5](https://github.com/dasaptaerwin/pemeringkatanpt2020/blob/master/REKAPITULASI/PPT-VOS-CITATION.pdf) Kedua, meskipun kutipan kumulatif dari artikel yang diterbitkan dalam jurnal meningkatkan faktor dampaknya, jumlah kutipan yang dimenangkan oleh masing-masing artikel mungkin tidak sesuai dengan faktor dampak jurnal yang menerbitkan artikel tersebut; Oleh karena itu, tidak tepat untuk mengukur kualitas sebuah artikel berdasarkan faktor dampak jurnal.

Banyak peneliti Indonesia tidak menyadari gerakan kutipan terbuka sedang diperjuangkan di tingkat global [6](https://theconversation.com/jalan-evolusi-bibliometrik-indonesia-104781) dan bahkan mungkin dengan sengaja mengabaikan fakta bahwa database komersial, yang dipromosikan oleh aspirasi untuk menjadi universitas kelas dunia, menerapkan sistem kutipan tertutup dengan efek yang tidak menguntungkan pada pengetahuan yang tersedia untuk umum.[7](https://doi.org/10.1162/qss_a_00023) Bagaimana kita dapat mengharapkan manfaat dari evaluasi penelitian berbasis kutipan jika karakteristik kutipan dan analisis kutipan - kriteria utama untuk evaluasi - tidak tercermin dengan baik oleh komunitas akademik, apalagi masyarakat umum?

Saat ini, ada empat inisiatif gerakan global lintas pemangku kepentingan untuk memastikan bahwa nilai beasiswa dalam masyarakat dinilai secara bertanggung jawab dan tepat. Keempat inisiatif tersebut adalah [Leiden Manifesto](http://www.leidenmanifesto.org/), [San Francisco Declaration on Research Assessment (DORA)](https://sfdora.org), [Gerakan Demokratisasi Pengetahuan](https://www.ei-ie.org/en/item/22729:democratising-knowledge-a-report-on-the-scholarly-publisher-elsevier), dan Proposal untuk menggantikan faktor dampak Jurnal. Hingga saat ini, lebih dari 2.200 organisasi dari lebih dari 90 negara telah menandatangani DORA.[8](https://www.cos.io/about/news/new-measure-rates-quality-research-journals-policies-promote-transparency-and-reproducibility) Penilaian lainnya, yaitu TOP Guidelines (singkatan dari Transparency and Openness Promotion),[9](https://onlinelibrary.wiley.com/doi/epdf/10.1111/dech.12637) juga telah diperkenalkan untuk mengecek kelengkapan prinsip-prinsip open-science di jurnal tersebut. tingkat. Terlepas dari kampanye yang kuat oleh para pendukung ilmu pengetahuan terbuka, kami percaya bahwa akan sulit untuk memantau lembaga-lembaga yang telah mendukung inisiatif ini dengan mendaftar karena Manifesto Leiden dan DORA didasarkan pada penilaian sendiri. Khususnya di beberapa negara yang kurang terwakili, jalan menuju penilaian penelitian tingkat nasional telah menjadi bahan perdebatan tingkat tinggi.

Kami mulai membahas perdebatan tersebut dengan memeriksa bibliometrik yang relevan di tingkat global dan kemudian mempertimbangkan implikasinya bagi Indonesia di tingkat nasional dan bagi komunitas ilmu bumi global di tingkat internasional.

Penelitian ini bertujuan untuk memastikan apakah basis data ilmiah yang berbeda memberikan hasil yang berbeda untuk subset publikasi yang berbeda seperti semua dokumen, hanya dokumen ilmu kebumian, hanya dokumen ilmu kebumian akses terbuka (OA), dan hanya dokumen bahasa Inggris tetapi dibatasi oleh konteks geografis. Hipotesisnya adalah sebagai berikut: Jika himpunan bagiannya berbeda secara signifikan, maka mengukur dampak suatu penelitian terutama berdasarkan perbedaan tersebut, atau peringkat, seperti yang ditunjukkan oleh database dan seperti yang dipraktikkan oleh banyak negara (termasuk Indonesia), akan menjadi tidak valid secara ilmiah dan diskriminatif secara sosial.

[[1-Abstrak]]
[[3-Metode]]
[[4-Hasil]]
[[5-Diskusi]]
[[6-Ucapan terima kasih]]
[[7-Pendanaan]]
[[8-Konflik kepentingan dan kontribusi]]
[[9-Daftar pustaka]]
[[10-Material pendukung]]