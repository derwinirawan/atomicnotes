---
tags: #wp4, #deltares
---

- develop [[socio-ecological resilience]] -> based on on close interactions between stakeholders
- [[wellbeing]]
- water quantity analysis -> water sensitivity
- [[inclusive governance]], [[policy brief]]
- [[knowledge-sharing]] -> [[project to training MOOC]],  elearning program