---
tags: #novelty, #relevance, #ideas, #ideation, #researchideas, #designthinking
---

- novelty
	- beyond time
- relevance
	- needs time
- [sweet spots] between novelty and relevance
	- eg: who would think we needed an iPod