# Tim kualitas air PT MADANI
- tags: #tobamaninjau, #africanlakes 
## Maninjau
- sampling 
	- air danau, air sungai
	- [[sedimen]]
	- [[major elements]], [[minor elements]], [[biological contents]]
- kedalaman sampai 168 m
- over capacity **[[keramba jaring apung]]** (KJA)
- tinggi kandungan belerang -> kenapa? -> apakah ada aktivitas volkanik?
- sungai intermiten
- pH air Danau Maninjau di permukaan lebih basa dibanding di bawah air -> [[tuba belerang]]
- [[DO]] tinggi -> kemampuan biota menyerap rendah
- EC tinggi -> tidak mampu diserap oleh biota
- rawan gerakan tanah
- terjadi pendangkalan -> di muara sungai
- krisis kepercayaan masyarakat -> banyak penelitian tapi tidak pernah disebarkan informasinya
- 
		
## Toba
- sampling sama dengan maninjau
- pH Toba di permukaan lebih rendah dibanding daerah yang lebih dalam
- DO lebih tinggi di selatan 
- EC lebih tinggi di permukaan

## QA
- tanya masalah sampling apakah sekali atau dua kali -> di luar lingkup, pengukuran hanya sekali -> [[data hidrogeologi]]
- masalah KJA -> di luar lingkup -> [[keramba jaring apung]]
- masalah data dan shp files -> [[my-logseq/pages/Prinsip F.A.I.R]]
- perlu membuat klasifikasi sumber limbah: domestik, industri dll; dan klasifikasi intensitasnya

# Hegar
- Kashem:
	- [[alluvial aquifer]]
	- [[fractured aquifer]]
	- ada pertimbangan untuk [[pumping test ]]untuk pemodelan

