---
tags: #wp4, #bima, #pontianak, #manado, #deltares
---

- pengembangan ke kota wisata internasional
- [[wisata bahari]]
- [[slump area]]
- sumber daya air:
	- jaringan irigasi, air baku. pengaman banjir
	- sungai -> spesifik ke [[Sungai Tondano]]
	- [[cekungan air tanah]]
	- konservasi
- [[kawasan strategis nasional]] [[KEK]]
- geologi
	- gunung api
	- aluvial (tidak luas)