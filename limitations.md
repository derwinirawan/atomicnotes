---
tags: #researchlimitation, #ideas, #ideation, #researchideas, #designthinking
---
- depends on
	- the sum of data
		- larger data -> less limitations
		- notice: anomaly, undistributed data
	- the scope
		- smaller scope -> more limitations 
		- but with more depth
	- the theory
		- general theory -> more limitations
		- specific theory -> less limitations

