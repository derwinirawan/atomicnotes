tags: #publicspeaking, #scicomm, #tedtalk 


## time
- war of attention
	- #rule-of-18-minutes
		- use 18 minutes bracket
	- #rule-of-3
		- narrow your messages into 3 most important things
- remember to re-engage
	- question
	- testing the audience 
	- tell another story
		- #RelevantStory
		- #CompellingStory
		- #ContinuousStory
	- structure
		- setup
		- conflict -> villains and heroes 
		- resolution




## practice
- practice a lot
- internalize the content
	- learn a lot
	- find examples (from your daily experience)
	- first you need to believe your content
- trials with different [[types of audience]]
-


## content
- [[tell stories]]
- [[use visuals]]
- [[create hooks]] between stories and visuals with the main messages
- #no-intentional-jokes
	- the best joke is non-intentional
	- the best joke is relevant
	- the best joke is self reflect
- #balance-of-things
	- technicals
	- stories
	- 
- [[simple slides]]
	- reduce clutter
	- 
- #memorablestory
	- form introvert to joining theatre club
	- 



## sources
- video 1
	- <iframe width="560" height="315" src="https://www.youtube.com/embed/ZNOQDp8v_wc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
- video 2
	- <iframe width="560" height="315" src="https://www.youtube.com/embed/iT2V-x4MXoc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
- video 3
	- <iframe width="560" height="315" src="https://www.youtube.com/embed/RbA2eHO_YdY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
- video 4
	- <iframe width="560" height="315" src="https://www.youtube.com/embed/sKUiE9DBkKc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
- video 5
	- <iframe width="560" height="315" src="https://www.youtube.com/embed/77FUr6ZsWjY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
- video 6
	- <iframe width="560" height="315" src="https://www.youtube.com/embed/QYcXTlGLUgE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
- video 7
	- <iframe width="560" height="315" src="https://www.youtube.com/embed/sh1-9xMZIfQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
- video 8
- video 9
- video 10
- 