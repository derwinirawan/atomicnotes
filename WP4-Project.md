---
tags: #wp4, #bima, #pontianak, #manado, #deltares
---

# Lingkup umum

## Dari proposal wp4

File -> [[proposal wp4-project]]


## Dari rapat koordinasi

File -> [[Project meeting-1-wp4 ]]



# Lokasi

## Manado

Files -> [[WP4-Manado]]

## Bima

Files -> [[WP4-Bima]]


## Pontianak

Files -> [[WP4-Pontianak]]

