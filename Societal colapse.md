---
tags: #extinction, #societycollapse, #geourban
---

# why society collapse

1. human impact to the environment
2. climate change (global change)
	- or earth disasters
		- volcanic eruptions
		- earthquakes
		- tsunamis, etc
	- pandemic 
3. relationship with neighboring society
4. relationship with hostile society

Relevant to [[MK-Geologi-Urban-Start]]


Video https://www.youtube.com/watch?v=IESYMFtLIis&t=432s

<iframe width="560" height="315" src="https://www.youtube.com/embed/IESYMFtLIis" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
