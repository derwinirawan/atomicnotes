- link: https://www.bbc.com/future/article/20201009-lake-kivu-the-african-lake-that-could-explode-with-methane
	- > Straddling the border between Rwanda and the Democratic Republic of the Congo, Kivu is one of a string of lakes lining the East African Rift Valley where the African continent is being slowly pulled apart by tectonic forces.
	- > These gases would ordinarily bubble out of the water, but Kivu’s great depth – more than 460 metres (1,500ft) at its deepest point – creates so much pressure that they remain dissolved.
	- ![[Pasted image 20210924052124.png]]
	- [[upwelling mechanisms]]?
	- how to track the origin of the gasses?
		- from surface process
		- from much deeper processes -> [[volcanism]] 

- tags: #tobamaninjau, #africanlakes 