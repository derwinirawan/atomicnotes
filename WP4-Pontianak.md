---
tags: #wp4, #deltares
---

- geology:
	- fluvial system
	- soft deposits
	- [[interaksi air tanah dengan air sungai]]
- groundwater
	- alluvial system
	- considerable reserve
- scope:
	- water quality
	- water contamination
		- domestic
		- industry

- flatter than the netherland
- expanding city to peatlands
- poverty -> have their own resilience
- sea level rise
- 