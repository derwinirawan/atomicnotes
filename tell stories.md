tags: #CompellingStory, #RelevantStory, #ContinuousStory, #tedtalk, #talkliketed, #publicspeaking


## structure
- setup
- conflict shows contrast
	- villains 
	- heroes
- resolution


## themes

### composition
- success
- failures
- balance



### great story means
- story -> makes you memorable
- memorable story -> makes you more memorable
- emotion is a fast lane to the brain
- create #commonground
	- it's a beautiful day, but I can't see it
	- to sympathy to empathy
	- had to be authentic and thoughful
	- no conflict means no story
	- good story keep audience engaged
- how to capture attention
- 





