---
tags: #seanos2021, #openscience, #scicomm 
---

# SEANOS VIDEOS
## Literature review
### [[SEANOS - Brian Nosek]]
- how to evaluate literature
- where to start
- what to do


## Trustworthy science
### [[SEANOS - Simine Vazire]]
- how research is evaluated
- the principles of trustworthy science
- how can we build one

## Team science and collaboration
### [[SEANOS - Nicholas Coles]]
- what is team science
- how and where to start
- the principles 

## Science policy
### [[SEANOS - Zakri Abdul Hamid]]
- why is it important
- how and where to start
- how to be a better scientist

## Science communication and science journalism
### [[SEANOS - Dasapta Erwin Irawan]]
- definition
- what to do
- where to start


### [[SEANOS - Dyna Rochmyaningsih]]
- what is journalism
- how do journalists work
- how researchers can participate to disseminate their results 

## Future of research and research integrity
### [[SEANOS - Gowri Gopalakhrisna]]
- how to safeguard #researchintegrity
- where do we go 
- science in the pandemic
- the roles of preprint

## Research data policy
### [[SEANOS - Su Nee Goh]]
- why we share data and how to do it
- principles: 
	- [[TOP Guidelines]] 
	- [[pages/Prinsip F.A.I.R]]
	- dealing with [[sensitive data]]
	- getting recognition by sharing data
- creating [[data management plan]]




# SEANOS LIVE DISCUSSIONS