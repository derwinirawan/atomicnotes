---
tags: #geourban
---

# Sejarah peradaban manusia
- Apakah itu peradaban
	- dihubungkan dengan budaya
	- biasanya dimulai ketika suatu bangsa sudah menetap
	- berkaitan dengan sumber daya yang melimpah
- Masa prasejarah
	- kuncinya adalah sejarah tertulis: ada atau tidak?
	- cave paintings
- Sejarah peradaban
	- lihat bagian berikutnya
- Video ini agak panjang, jadi bisa Anda lompat-lompat saja. Tapi perhatikan apa yang dimaksud dengan **“peradaban”**.
- [https://youtu.be/wyzi9GNZFMU](https://youtu.be/wyzi9GNZFMU)
- 

# Geologi dalam pembangunan konstruksi kawasan urban
- Aspek geologi teknik dan pemetaan bawah permukaan kawasan metropolitan
- Bahan bangunan
- Ekskavasi dan pengerukan kawasan urban

## Regulasi

### RUU Tata Ruang Bawah Bumi

Simak presentasi saya terkait dengan Rancangan UU Tata Ruang Bawah Bumi 

-   tautan [https://derwinirawan.wordpress.com/2019/04/09/kehidupan-bawah-tanah/](https://derwinirawan.wordpress.com/2019/04/09/kehidupan-bawah-tanah/) 
-   salindia ada di tautan di atas
-   video langsung lompat ke menit 7:00 saja
-   [tinjauan yuridis](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwjUq9icnqDzAhXEb30KHQWIBroQFnoECAgQAQ&url=http%3A%2F%2Fjurnal.dpr.go.id%2Findex.php%2Fhukum%2Farticle%2Fdownload%2F194%2F135&usg=AOvVaw3s-r2w7DnIb70NfbCzzYGM) 

### UU Penataan Ruang
[UU No. 26/2007 tentang Penataan Ruang](https://jdih.kemenkeu.go.id/fullText/2007/26TAHUN2007UU.htm)


# Geologi dan pertanian kawasan urban
- Geokimia tanah dan hubungannya dengan air tanah dan 
- Penggunaan pupuk
- Kontaminasi air tanah
- Sumber kontaminasi:
	- pertanian
	- perkotaan
	- dll

# Hidrogeologi kawasan urban
- Dampak pembangunan infrastruktur kawasan urban
- Manajemen air tanah
	- Bagaimana seharusnya
	- Bagaimana realisasinya