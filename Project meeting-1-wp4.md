---
tags: #wp4, #bima, #pontianak, #manado, #deltares
---

Date/time: 2021-09-29
Participants: 
* **Radboud Univ - Deltares**
	* Edwin de Jong
	* Deltares - Ansje Lohr
	* Deltares - Bastien van Veen
	* Deltares - Gertjan Geerling
* **ITB**
	* ITB - Prasanti Widyasih Sarli - Asih
	* ITB - Prayatni Soewondo
	* ITB - Faizal Rohmat
	* ITB - Farhan
	* ITB - Ahmad Setiawan
	* ITB - Tery Setiawan
	* ITB - Fauzan WR
	* ITB - Vinca Alvando
	* ITB - Wika Maulany Fatimah
* **UI**
	* UI - Bagus Takwin
	* UI - Paksi Walandouw
	* UI - Dewa Wisana
* **UPI**
	* UPI - Dede Rohmat
* **HIVOS**
	* Hivos - Ilham Saenong
* **LAPAN**
	* LAPAN - Wendi Harjupa
* **Climate Justic**e
	* Climate Justice - Sandra Winarsa
	* Climate Justice - Eva Sulistiawaty
* **Universitas Kristen Maranatha**
	* UKM - Missiliana
* **ITERA**
	* ITERA - Dion Awfa

# objectives wp-4
- Files -> [[objectives wp-4]]


# characters
- multidisciplines
- ToC model 
- potential stakeholders and actual stakeholders -> engagement -> [[co-creative process]] 


# methods
- selection of area
- methods
	- quantitative
	- qualitative
	- mixed method
- data storage
	- data management
	- data distribution
	- data access
		- open
		- restrictive

# planning-management
- pandemic issues
- data collection
- meetings
	- board
	- seminars
	- stakeholders
- dissemination, PR, website

# previous project

- Files -> [[urban flood resilience]]


## demographic 
Files -> [[wp-demographic]]



