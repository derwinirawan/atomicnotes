---
tags: #seanos2021, #openscience, #scicomm, #literature 
---

# Interview with Brian Nosek for #SEANOS2021

## how to start finding relevant work
- starting out a research from scratch with ocean of literature (countless papers), where do we start?
- finding out our role in research to engage more with the literature [[engage with literature]]
- breaking down the parts of a research:
	- innovation: problem-solving -> related to
		-  [[pages/innovation]] 
		-  [[pages/Innovation without patents]]
		-  [[pages/Paten dan inovasi]]
	- [[verification]]: 
		- understanding, 
		- testing, 
		- questioning by comparing with [what your current knowledge], 
		- interograting literature, 
		- wrestle, 
		- poke

- which question that interests us
	- to direct our attention
- what do people know about area of interests
	- who and how do they study that -> [[review papers]] 
	- starting from problem or issue and to seek solution
- read a seminal/famous paper and seek blank spots
	- areas to fill in
	- seek other ways
	- start from other point of view -> [[research point of view]]


## rigorous and replication
- rigor still can be wrong
- detecting something not right
	- should I believe it
	- learning to be [[skeptical in research]]
	- run the study again [[pages/reproduksi dan replikasi]]
		- important so others can redo the study to evaluate it

- reading papers together
	- with like-mind colleagues 
	- to detect uncertainty
	- argument structure
	- strength

- extraordinary claims need extraordinary evidence -> important feature of skepticism in science -> [[skeptical in research]]
- 

