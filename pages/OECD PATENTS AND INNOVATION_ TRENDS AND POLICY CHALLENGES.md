title:: OECD PATENTS AND INNOVATION: TRENDS AND POLICY CHALLENGES
public:: true

# Patents increasingly role
	- innovation and
	- economic
- # Shifting of innovation processes
	- less on individual firms and
	- more on interactions global networks
		- public and
		- private sectors.
# Changes in patent policy aiming
	- encouraging investments in innovation and
	- enhancing the dissemination of knowledge.
- # Unresolved issues
	- ## Markets for technology
		- important for the circulation of knowledge.
		- a pivotal role in the development of technology transactions.
		- Governments need to improve their knowledge of
			- the functioning of markets for technology and
			- the effect of such markets on economic performance in order
			-
	- ## Encouraging patenting by public research organisations (PROs)
		- #weird
		- has led to increased commercialisation of inventions derived from publicly funded research #weird
			- hence generating greater benefits to society
			- but may have made it more difficult for researchers to access certain types of basic science.
		- Governments should
			- explore ways to encourage alternative means of disseminating knowledge, such as the public domain, and to improve the diffusion of patented inventions
	- ensure access to basic inventions,
		- by monitoring patenting and licensing practices at PROs, and
		- by reinstating and clarifying the exemption for research use, which is now being restricted.
	- ## in the context of Software and services
		- The role of patents in the expanding world of open source software also needs to be evaluated.
# Evaluation of patent system
	- Economic evaluation suggests that there are further possible directions of change for patent regimes that are worth exploring.
		- introducing a more differentiated approach to patent protection that depends on specific characteristics of the inventions, such as their life cycle or their value (as opposed to the current uniform system);
		- making patent fees commensurate to the degree of protection provided;
		- and developing alternatives to patenting, **such as the public domain**.
		-
- # Greater challenges of patents system
	- increased globalisation,
	- the overwhelming use of Internet as a vehicle of diffusion, and
	- expanded innovation in services.
- [[Innovation without patents]]
-