title:: Prinsip F.A.I.R

- **F**`indable`
- **A**`ccessible`
- **I**`nteroperable`
- **R**`usable`
- [Ref](https://www.go-fair.org/fair-principles)