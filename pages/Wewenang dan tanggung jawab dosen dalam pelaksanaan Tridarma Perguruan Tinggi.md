# Kewenangan dan tanggung jawab mengajar
- 1) Dosen dengan jabatan Asisten Ahli yang bergelar magister hanya dapat mengajar program
  sarjana, dan yang bergelar doktor dapat mengajar program sarjana serta membantu
  pengajaran program magister dan doktor.
- 2) Dosen dengan jabatan Lektor yang bergelar magister hanya dapat mengajar program
  sarjana, sedangkan yang bergelar doktor dapat mengajar program sarjana dan magister
  serta membantu pengajaran program doktor.
- 3) Dosen dengan jabatan Lektor Kepala yang bergelar magister hanya dapat mengajar program
  sarjana, sedangkan yang bergelar doktor dapat mengajar program sarjana, magister, dan
  doktor.
- 4) Dosen dengan jabatan Profesor dapat mengajar program sarjana, magister, dan doktor.
-
# Kewenangan dan tanggung jawab membimbing
- 1) Dosen dengan jabatan Asisten Ahli dan bergelar magister hanya dapat membimbing
  mahasiswa program sarjana, sedangkan yang bergelar doktor dapat membimbing
  mahasiswa program sarjana serta menjadi pembimbing pendamping program magister.
  2) Dosen dengan jabatan Lektor dan bergelar magister hanya dapat membimbing mahasiswa program sarjana, sedangkan yang bergelar doktor dapat membimbing mahasiswa sarjana
  dan magister serta membantu pembimbingan program doktor.
  3) Dosen dengan jabatan Lektor Kepala yang bergelar magister hanya dapat membimbing
  mahasiswa program sarjana, sedangkan yang bergelar doktor dapat membimbing program sarjana dan magister serta membantu pembimbingan program doktor. Dosen dengan jabatan Lektor Kepala yang bergelar doktor dan pernah menulis paling sedikit satu makalah di jurnal ilmiah internasional bereputasi sebagai penulis pertama dapat membimbing program doktor.
  4) Dosen dengan jabatan Profesor dapat membimbing program sarjana, magister, dan doktor.