title:: publikasi terkait mahasiswa S2/S3

- publikasi yang terkait mahasiswa S2/S3 akan memiliki beberapa potensi kendala publikasi
	- waktu publikasi bisa sampai setahun bahkan lebih
	- apalagi bila dihubungkan dengan riset, maka hasil riset harus sudah masuk paling lambat
		- semester tiga (untuk mahasiswa S2) dengan asumsi studi 4 semester (dua tahun) atau
		- semester lima (untuk mahasiswa S3) dengan asumsi studi 6 semester (tiga tahun)
	- agar hasil riset bisa masuk pada waktu di atas, maka mahasiswa S2/S3 harus sudah memulai riset
		- sejak mereka pertama masuk
		- ini tidak masalah, tapi apakah dosen dan sistem kurikulum kita sudah memungkinkan untuk itu?
		- apalagi untuk S3 dengan berbagai tahapan administrasi di tahun awal kuliah (ujian kualifikasi, ujian proposal dll)
		- S3 juga masih perlu mengikuti kuliah
- selain bidang-bidang FSRD (sains sosial), ada bidang rekayasa yang memiliki waktu publikasi relatif yang lama, apalagi bila dikaitkan dengan status kuartil scimago (yang dipersyaratkan)
	- ini akan berkaitan dengan dengan apakah dosen masih cukup waktu dan material untuk dapat menghasilkan naskah yang layak terbit di jurnal Q1 (atau Q2)
	- ditambah lagi dengan [[potensi bias]] yang sangat mungkin dimiliki reviewer jurnal tersebut