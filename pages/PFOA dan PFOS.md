public:: true

- PFOA dan PFOS ([Source: EPA](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwjd9NvM_aTyAhVDJHIKHQ3TB74QFnoECAYQAw&url=https%3A%2F%2Fwww.epa.gov%2Fsites%2Fdefault%2Ffiles%2F2017-12%2Fdocuments%2Fffrrofactsheet_contaminants_pfos_pfoa_11-20-17_508_0.pdf&usg=AOvVaw2oiw2wof_pw21KaeU2ocoe))
	- perfluorooctane sulfonate (PFOS) and perfluorooctanoic acid (PFOA)
	- including physical and chemical properties; environmental and health impacts; existing federal and state guidelines; detection and treatment methods; and additional sources of information. This fact sheet is intended for use by site managers who may address these chemicals at cleanup sites or in drinking water supplies and for those in a position to consider whether these chemicals should be added to the analytical suite for site investigations.
-