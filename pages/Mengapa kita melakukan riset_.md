title:: Mengapa kita melakukan riset?
public:: true

- #metlit
# Berawal dari
	- ketidaktahuan vs keingintahuan
	- masalah yang jelas vs masalah yang tidak jelas
	- kekurangpuasan dengan hasil riset sebelumnya
-
# Masalah
	- Pribadi
		- berhubungan dengan profesi
		- berhubungan dengan keilmuan
		- berhubungan dengan minat
		- dll
	- Kelompok
		- laboratorium
		- universitas
		- asosiasi
		- bidang ilmu tertentu
		- dll
	- Masyarakat secara umum
		- dunia
		- nasional
		- lokal
		- dll
-
-
# Definisi
	- Re-search
		- Re = mengulang
		- search = mencari
		- mencari ulang
	- adalah kegiatan `sistematis` untuk mencari data dan menganalisisnya untuk menghasilkan `jawaban` atau `alternatif jawaban`atas suatu `masalah`.
	-