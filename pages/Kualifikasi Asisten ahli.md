- Dosen yang mengusulkan jabatan Asisten Ahli harus memenuhi kualifikasi dan kriteria:
	- 1) memiliki ijazah serendah-rendahnya magister dari program studi atau perguruan tinggi
	  terakreditasi dalam bidang ilmu yang sesuai dengan bidang ilmu penugasannya;
	- 2) memiliki pengalaman membantu penyelenggaraan pengajaran bagi mahasiswa diploma
	  dan/atau sarjana;
	- 3) mampu menyebarluaskan ilmu pengetahuan, teknologi, dan/atau seni;
	- 4) mampu memahami pelaksanaan penelitian dan pengabdian kepada masyarakat;
	- 5) mampu menulis karya ilmiah yang dipublikasikan sebagaimana diatur dalam Pedoman
	  Operasional Penilaian Angka Kredit Kenaikan Jabatan Akademik/Pangkat Dosen yang
	  berlaku;
	- 6) untuk bidang seni dan arsitektur, persyaratan no 5) dapat digantikan dengan rancangan,
	  disain, dan/atau karya seni yang diakui setara oleh komunitasnya; dan
	- 7) memiliki kinerja, integritas, tanggung jawab pelaksanaan tugas, etika, dan tata krama
	  dalam kehidupan kampus.