- Dosen yang mengusulkan jabatan Lektor harus memenuhi kualifikasi dan kriteria:
  1) memiliki ijazah serendah-rendahnya magister dari program studi atau perguruan tinggi
  terakreditasi dalam bidang ilmu yang sesuai dengan bidang ilmu penugasannya; 2)mampu menerapkan proses pembelajaran dan pembimbingan secara mandiri bagi mahasiswa diploma dan/atau sarjana untuk yang sudah menduduki jabatan asisten ahli; atau memiliki pengalaman membantu penyelenggaraan pengajaran bagi mahasiswa diploma dan/atau sarjana bagi yang sudah memiliki ijazah doktor dan belum menduduki
  jabatan akademik;
  3) mampu memahami teori bidang ilmu yang menjadi penugasannya;
  4) mampu menerapkan teori bidang ilmu yang menjadi penugasannya dalam pelaksanaan
  penelitian dan pengabdian kepada masyarakat;
  5) mampu menulis karya ilmiah yang dipublikasikan sebagaimana diatur dalam Pedoman
  Operasional Penilaian Angka Kredit Kenaikan Jabatan Akademik/Pangkat Dosen yang
  berlaku;
  6) untuk bidang seni dan arsitektur, persyaratan no 6) dapat digantikan dengan rancangan,
  disain, dan/atau karya seni yang diakui setara oleh komunitasnya; dan
  7) memiliki kinerja, integritas, tanggung jawab pelaksanaan tugas, etika, dan tata krama
  dalam kehidupan kampus.