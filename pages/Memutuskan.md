- lihat lampiran
# Latar belakang
- tugas dosen sesuai [[Undang-Undang RI No. 12 Tahun 2012 Tentang Pendidikan Tinggi pasal 12 peran dosen]]
- [[sudut panjang kenaikan pangkat/jabatan dari sisi dosen dan ITB]]
	-
	-
# Kualifikasi dosen
- [[Kualifikasi Asisten ahli]]
- [[Kualifikasi Lektor]]
- [[Kualifikasi Lektor Kepala]]
- [[Kualifikasi Guru Besar]]
-
# Prinsip proses penilaian
-
  1. Adil, setiap usulan diperlakukan sama dan dinilai dengan kriteria penilaian yang sama.
-
  2. Obyektif, penilaian dilakukan terhadap bukti-bukti yang diusulkan dan dapat dipertanggungjawabkan kebenarannya, serta dinilai dengan kriteria penilaian yang jelas.
-
  3. Akuntabel, pertimbangan dan hasil penilaian dapat dijelaskan dan dipertanggungjawabkan.
-
  4. Transparan dan bersifat mendidik, proses penilaian dapat dimonitor dan dikomunikasikan, serta dengan menjunjung tinggi prinsip-prinsip dalam proses pembelajaran bersama, untuk mendapatkan proses yang efektif dan efisien dengan hasil yang benar dan lebih baik.
-
  5. `**Otonom**` dan bermutu.
	- ITB berwenang melakukan penilaian dan penetapan angka kredit jabatan akademik Asisten Ahli dan Lektor.
	- Untuk usulan kenaikan jabatan akademik ke Lektor Kepala dan Profesor, serta kenaikan pangkat dalam jabatan akademik Lektor Kepala dan Profesor, ITB diberi kewenangan untuk menilai
		- komponen Pendidikan, Penelitian, Pengabdian kepada masyarakat, dan unsur penunjang,
		- khusus untuk komponen penelitian dan karya ilmiah sains/teknologi/seni proses penilaian akhir dan penetapan angka kredit akhir dilakukan oleh Direktorat Jenderal Sumber Daya Iptek dan Dikti.