public:: true

- # aboutme
	- [@dasaptaerwin](https://twitter.com/dasaptaerwin)
	- [ORCID](https://orcid.org/0000-0002-1526-0863)
	- maintaining [@rinarxiv](https://twitter.com/rinarxiv) from [@itbofficial](https://twitter.com/itbofficial)
	- [I think therefore I draw](https://bit.ly/wikimediadasaptaerwin)
- # Preprint = Content over packaging
  collapsed:: true
	- ![image.png](../assets/image_1629410899945_0.png)
	- packaging over content
	- lesson learnt from Indonesia
	- where most of the research funding came from the government
	- but they don't ask anything but the published journal article (#transparency)
	-
- # Some of key points of preprinting
  collapsed:: true
	- ![William Wallace](https://upload.wikimedia.org/wikipedia/commons/4/4a/William_Wallace_and_the_copyrights.jpg)
	- [[freedom]]: content and layout and [[ownership]]: keeping what's yours,
	- [[transparency]]: sharing, reporting, performance indicator,
	- [[content over packaging]]: the paper will be judged based on the content,
	- [[accessibility]]: [[Green OA]] should be a way to get [Sustainable Academic Goals]
	- [[to engage]]
- # Where are we now?
  collapsed:: true
	- ![Virtual Insanity](https://upload.wikimedia.org/wikipedia/commons/3/37/Virtual_Insanity.jpg)
	- 58 preprints since May 2020
	- 223 users
		- 148 authors
	- Tons of outreach events in collaboration with:
		- National:
			- PDDI LIPI
			- Wikimedia Indonesia
			- Creative Commons Indonesia
			- etc
		- International:
			- Openscience South East Asia
			- [Open Access India](https//openaccessindia.org/)
			- [AsapBio](https://asapbio.org)-[PreReview](https://prereview.org )
			- [PeerCommunityIn](https://peercommunityin.org)
			- etc
		-
- # Where are we going?
  collapsed:: true
	- ![Money flows](https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/The-stakeholders-of-science-and-the-flow-of-money.jpg/1920px-The-stakeholders-of-science-and-the-flow-of-money.jpg)
	- [[reducing dependencies to commercial entities]]
	- more engagements on:
		- [[open peer review]] and open data
		- [[open access policy]]
		- [[education for undergraduates and early career scientists]]
		-