public:: true

- Data penting. Bukan hanya untuk riset ybs juga untuk riset selanjutnya.
- Modalnya adalah data mentah dengan dokumentasi lengkap yang dapat ditemukan dan diakses secara daring. [[Prinsip F.A.I.R]]
- Sumber data:
	- masih berupa data mentah atau
	- sudah mengandung hasil analisis
- Posisi riset tugas akhir:
	- apakah berdasarkan data baru
	- apakah berdasarkan data lama dengan analisis/interpretasi baru (reproduksi)
	- apakah berdasarkan data lama dan mengulang analisis/interpretasi lama (replikasi)
		- tentang [[reproduksi dan replikasi]]
- [[Data citation]]