- a. bahwa dalam rangka melaksanakan misi Institut Teknologi Bandung, maka diperlukan manajemen jenjang karir Dosen yang baik;
  b. bahwa telah terjadi perubahan Pedoman Operasional Penilaian Angka Kredit
  Kenaikan Jabatan Akademik/Pangkat Dosen yang dikeluarkan oleh Dirjen Dikti;
  c. bahwa telah terbit Surat Edaran Direktur Jenderal Pendidikan Tinggi Nomor 4 Tahun 2021 tentang Pedoman Operasional Penilaian Angka Kredit Kenaikan
  Jabatan Akademik/Pangkat Dosen Tahun 2019;
  d. bahwa berdasarkan pertimbangan sebagaimana dimaksud pada huruf a, b, dan
  c di atas, perlu mengatur tentang Penilaian dan Persetujuan Usulan Penetapan Awal dan Kenaikan Jabatan Akademik/Pangkat Dosen Institut Teknologi Bandung dengan Peraturan Senat Akademik ITB.