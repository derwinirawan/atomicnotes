- Ref: Keukeu Rosada [15:09, 8/10/2021]
- Bedanya kalau coliform itu kelompok bakteri yang bentuknya batang (pendek) dan bisa memfermentasi laktat... Genusnya al: Citrobacter, Enterobacter, Hafnia, Klebsiella,  dan Escherichia. Biasa ditemukan di lingkungan: air, tanah dan udara (kalau terbawa angin)
  [15:09, 8/10/2021] Keukeu Bio Unpad: E. coli sendiri adalah Escherichia coli... salau satu jenis dari Genus  Escherichia...
- namanya coli... berasal dari kata colon atau usus besar... jadi bakteri tsb merupakan flora normal/bakteri yang hidup secara normal di usus mamalia atau hewan berdarah panas termasuk manusia.
- fungsi E. coli di usus besar adalah memproduksi vit K dan membantu pembusukan sisa makanan sebelum dibuang.. nah jadi bisa saja terdapat dalam feses ketika dibuang ke lingkungan... nah dia bisa jadi indikator pencemaran lingkungan oleh feses (bahan organik domestik khususnya)
- kalau dalam deteksi ya.... kalau E. coli ada berarti coliform ada karena E. coli bagian dari coliform... tapi kalau coliform ada belum tentu ada E. colinya.
- jadi kalau di metode analisis MPN, tahap 1 untuk melihat coliform, tahap 1 untuk melihat ada tidak E.colinya.
- agar air yang mengandung e.coli/coliform bisa dikonsumsi asal dimasak.
- Sebetulnya secara umum E. coli dan coliform bukan bakteri patogen (walaupun ada yang jenis2 yang termutasi menjadi virulen)... bakteri tsb dijadikan indikator pencemaran dan keberadaan bakteri patogen lainya.