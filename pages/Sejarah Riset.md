public:: true

- https://www.etymonline.com/word/research
	- the [[systematic]] investigation into and study of materials and sources in order to establish facts and reach new conclusions.
	- investigate systematically
	  late 16th century: from obsolete French recerche (noun), recercher (verb), from Old French re- (expressing intensive force) + cerchier ‘to search’.
	- 1570s, "act of searching closely" for a specific person or thing, from French recerche (1530s, Modern French recherche), back-formation from Old French recercher "seek out, search closely" (see research (v.)).
	- The meaning "diligent scientific inquiry and [[investigation]] directed to the [[discovery]] of some fact" is attested by 1630s. The general sense of "investigations into things, the habit of making close investigations" is by 1690s. The phrase [[research and development]] for "work on a large scale toward [[innovation]] " is recorded from 1923.
	- 1590s, "investigate or study (a matter) closely, [[search]] or [[examine]] with [[continued]] care," from French recercher, from Old French recercher "seek out, search closely," from re-, here perhaps an intensive prefix (see re-), + cercher "to seek for," from Latin circare "go about, [[wander]], traverse," in Late Latin "to [[wander]] hither and thither," from circus "circle" (see circus).
	-