public:: true

- Moser (2011)
# Results
	- ## Patenting rates vary most significantly across industries
		- British
			- Patenting rates are highest in machinery: In 1851,
				- 30 percent of British exhibits in manufacturing machinery were patented,
				- 25 percent of exhibits of engines, and
				- 20 percent of agricultural machinery.
				- only 5 percent of chemical exhibits were patented,
				- 5 percent of exhibits in mining and metallurgy,
				- 7 percent of exhibits in textiles, and
				- 8 percent in food processing.
			- Innovations in all of these industries were difficult to copy.
			- Improvements in dyeing and bleaching featured prominently among 19 -century innovations in textiles; processes of preservation and secret recipes, such as the one for Coca-Cola, made up most innovations in food processing. Innovations in metallurgy depended on craft-based skills that proved impossible to imitate (Harris 1976, p. 49, Landes 1983, p. 283).
	- Regression results confirm that variation across industries is the most powerful predictor of patenting decisions. Marginal effects imply that exhibits of manufacturing machinery were 21 percent more likely to be patented (significant at 1 percent, Table VI, columns 1-2, compared with manufactures as a control).
	- Regressions of the U.S. data also confirm that
		- high-quality innovations were significantly more likely to be patented.
		- Award-winning exhibits from the United States were between 12 and 14 percent more likely to be patented (significant at 1 percent).
	- Data on changes over time in the lag between patenting and invention suggest that inventors waited up to 40 years to patent dyes that had been discovered between 1851 and 1876.
	- patenting decisions
		- where the relative strength of patents and secrecy varies across industries, and
		- differences in the profitability of innovations amplify the effects of variation in the effectiveness of secrecy (not reported).
# Conclusions
	- Introduces a unique historical data set of more than 8,000 British and American innovations at world fairs between 1851 and 1915 to explore the relationship between patents and innovations.
	- The data indicate that the majority of innovations - 89 percent of British exhibits in 1851 - are not patented.
	- occur across industries; inventors are most likely to use patents in industries where innovations are easy to reverse-engineer and secrecy is ineffective relative to patents.
	- In the late 19 -century, scientific breakthroughs, including the publication of the periodic table, lowered the effectiveness of secrecy in the chemical industry.
-