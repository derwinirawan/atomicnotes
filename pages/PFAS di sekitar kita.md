public:: true

# Apakah PFAS itu?
- PFAS adalah Per- and polyfluoroalkyl substances (PFAS), yaitu bahan kimia buatan manusia, termasuk di dalamnya adalah PFOA, PFOS, GenX, dan bahan kimia lainnya ([USEPA-PFAS](https://www.epa.gov/pfas/basic-information-pfas)).
- Telah diproduksi sejak lama (1940an di AS), PFAS telah digunakan di berbagai industri di seluruh dunia. Dari PFAS, yang paling banyak diproduksi adalah jenis [[PFOA dan PFOS]]. Keduanya sangat tahan terhadap perubahan, tidak meluruh, sehingga akan terus terkumpul sejalan dengan waktu bila terus-menerus diproduksi. Berbagai studi ilmiah telah membuktikan bahwa PFAS akan memberikan dampak merusak kepada kesehatan tubuh manusia bila terpapar.
# Apa yang mengandung PFAS?
- Yang mengandung PFAS diantaranya:
	- Kemasan makanan mengandung PFAS,
	- Makanan yang dikemas dengan kemasan mengandung PFAS atau makanan yang dibudidayakan dengan tanah/media yang mengandung PFAS,
	- Alat rumah tangga yang mengandung lapisan anti lengket/_non-stick_ (misal: teflon) atau anti air/_water-repellent_, sampai ke busa pemadam api,
	- Beberapa industri yang bekerja atau berhubungan dengan pelapisan krom, elektronik, dan perminyakan (_oil recovery_) yang menggunakan zat mengandung PFAS,
	- Air minum yang berdekatan dengan industri yang bekerja dengan zat yang mengandung PFAS, tempat pembuangan sampah, tempat pengolahan air limbah, fasilitas pemadam kebarakan),
	- Organisme yang makan atau minum zat yang mengandung PFAS.
- ![PFAS diagram](https://www.euchems.eu/newsletters/wp-content/uploads/2020/08/PFASSources.png)
- ![Infografis PFAS](https://www.env-health.org/wp-content/uploads/2021/05/FIGO-PFAS-Infographic-FINAL-4.27.2021-scaled.jpg)
- ![Struktur molekul PFAS](https://www.vrwa.org/Portals/43/EasyDNNnews/50871/img-pfas-diagram2-1024x666.png)
# Apa saja dampaknya?
- Dampak terpapar PFAS dalam jumlah tertentu ([sumber: US-EPA](https://www.epa.gov/ground-water-and-drinking-water/drinking-water-health-advisories-pfoa-and-pfos)):
	- mengganggu perkembangan janin atau mengganggu pembentukan ASI (Air Susu Ibu),
	- mengganggu pertumbuhan anak atau remaja, misal pertumbuhan tulang,
	- menyebabkan kanker, gangguan hati, gangguan jaringan,
	- menurunkan imun,
	- dll
-
# Rujukan lainnya
	- https://www.epa.gov/ground-water-and-drinking-water/drinking-water-health-advisories-pfoa-and-pfos
	- https://www.alodokter.com/ada-bahan-beracun-yang-muncul-dari-alat-masak-antilengket
	- https://oem.bmj.com/content/75/1/46.abstract
-