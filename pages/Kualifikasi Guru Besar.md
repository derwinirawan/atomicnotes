- Dosen yang mengusulkan jabatan Profesor harus memenuhi kualifikasi dan kriteria:
	- 1) berijazah doktor dari program studi atau perguruan tinggi terakreditasi dalam bidang ilmu
	  yang sesuai dengan penugasan dan karya ilmiah/penelitiannya;
	- 2) mampu mendidik secara profesional;
	- 3) mampu menerapkan dan mengembangkan proses pembelajaran dan materi/buku ajar,
	  serta bagi mahasiswa sarjana, magister, dan doktor;
	-
	  background-color:: #978626
	  > 4)pernah menjadi anggota tim pembimbing mahasiswa doktor dan menjadi penguji disertasi;
	- 5) mampu menganalisis teori bidang ilmu yang menjadi penugasannya;
	- 6) mampu menerapkan dan menganalisis teori bidang ilmu yang menjadi penugasannya
	  dalam pelaksanaan penelitian dan pengabdian kepada masyarakat;
	- 7)memenuhi persyaratan khusus dan tambahan sebagaimana diatur dalam Pedoman Operasional Penilaian Angka Kredit Kenaikan Jabatan Akademik/Pangkat Dosen yang
	  berlaku;
	-
	  > 8) selain untuk memenuhi persyaratan khusus pada butir 7) telah mempublikasikan hasil penelitiannya dalam jurnal internasional bereputasi sebagai penulis utama setelah kenaikan jabatan terakhir paling sedikit 2 buah; dan
	- 9) memiliki kinerja, integritas, tanggung jawab pelaksanaan tugas, etika, dan tata krama dalam kehidupan kampus.