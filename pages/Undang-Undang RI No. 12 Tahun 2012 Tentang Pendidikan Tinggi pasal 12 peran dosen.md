title:: Undang-Undang RI No. 12 Tahun 2012 Tentang Pendidikan Tinggi pasal 12 peran dosen

- tugas dosen:
	- **sebagai anggota sivitas akademika**: mentransformasikan ilmu pengetahuan dan/atau teknologi yang dikuasainya kepada mahasiswa dengan mewujudkan suasana belajar dan pembelajaran sehingga mahasiswa aktif mengembangkan potensinya.
	- **sebagai ilmuwan**:
		- mengembangkan suatu cabang ilmu pengetahuan dan/atau teknologi melalui penalaran dan penelitian ilmiah serta menyebarluaskannya.
		- (secara perseorangan atau berkelompok) wajib menulis buku ajar atau buku teks, yang diterbitkan oleh perguruan tinggi dan/atau publikasi ilmiah sebagai salah satu sumber belajar dan untuk pengembangan budaya akademik serta pembudayaan kegiatan baca tulis bagi bagi sivitas akademika.
		- [[Wewenang dan tanggung jawab dosen dalam pelaksanaan Tridarma Perguruan Tinggi]] khususnya dalam mengajar dan membimbing melekat pada diri dosen sesuai dengan jabatan akademik yang disandangnya.
		-
-