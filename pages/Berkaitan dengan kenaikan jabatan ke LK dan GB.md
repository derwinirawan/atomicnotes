## Draft peraturan SA
	- ![draft-SA.jpeg](../assets/draft-SA_1628673014218_0.jpeg)
## Ke LK
	- wajib menerbitkan minimum satu buah artikel di [[jurnal internasional bereputasi]]
	- peraturan nasional (sepertinya) minimum adalah satu artikel pada jurnal nasional terakreditasi.
		- faktanya adalah ketika seorang dosen memilih menerbitkan pada artikel pada jurnal nasional terakreditasi, **sering kali masih dipertanyakan**.
		- jadi (dulu) di ITB (dan banyak kampus) ada himbauan untuk menerbitkan artikel pada jurnal yang statusnya lebih tinggi dari jurnal nasional terakreditasi. pilihannya (kala itu) adalah:
			- prosiding seminar internasional terindeks scopus -> kalau itu dinilai setara dengan jurnal internasional **biasa**.
				- sekarang di ITB (dan kampus lain) juga sudah dipertanyakan, karena dianggap tidak layak dan dianggap mudah terbitnya.
			- jurnal internasional yang tidak terindeks scopus
				- sering kali juga dipertanyakan, ketika jurnal tersebut adalah jurnal nasional
			- jurnal internasional terindeks scopus, sub pilihannya dua
				- [[yang ber IF atau masuk kuartil scimago]]
				- yang tidak ber IF dan tidak masuk kuartil scimago
				- yang tidak ber IF tapi masuk kuartil scimago
	- dari sekian banyak pilihan, yang diyakini aman adalah [[yang ber IF atau masuk kuartil scimago]]
## Ke GB
	- Sejak awal (menurut aturan dikti) untuk ke GB memang dibutuhkan artikel di [[jurnal internasional bereputasi]] dan
	- penulis pertama juga (harus) sekaligus menjadi penulis korespondensi agar dapat diberi skor maksimum
		- kenapa ini penting?
		- karena dalam aturan dikti yang baru, [[penulis korespondensi]] bisa saja bukan penulis pertama
		- dan untuk [[penulis korespondensi]] diberi skor juga (sebelumnya tidak)
		-
		-
	- terkait jumlah. perlu kejelasan perlu satu atau dua dan apakah ini syarat aman yang punya keberhasilan tinggi, ataukah ini syarat yang di jakarta masih mungkin diutak-atik secara subyektif oleh tim PAK jakarta.
	- inti pesannya adalah: hidup dosen ITB sudah cukup rumit tanpa ada tambahan peraturan tambahan yang baru