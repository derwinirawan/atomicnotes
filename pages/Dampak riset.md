public:: true

# Internal
	- ## untuk peneliti
		- pengakuan
		- pangkat/jabatan
		- dana riset
		- kepuasan pribadi
	- ## untuk lembaga
		- pengakuan
		- pemeringkatan
		- dana riset
		- nilai jual
	- ## untuk negara
		- pengakuan
		- pemeringkatan
		- nilai jual
# Eksternal
	- ## untuk peneliti lain
		- memudahkan mereka menjalankan riset mereka
		- memudahkan mereka mengembangkan ilmu pengetahuan
		- memudahkan mereka melanjutkan riset Anda
	- ## untuk lembaga lain
		- memudahkan mereka menjalankan riset mereka
		- memudahkan mereka mengembangkan ilmu pengetahuan
		- memudahkan mereka melanjutkan riset Anda
	- ## untuk negara lain
		- membawa hasil lokal ke ranah internasional
		- [[Scicomm]]
		- [[jurnal internasional bereputasi]]???
		- solusi lokal mungkin bermanfaat untuk masalah yang sama di negara lain
	- ## masyarakat umum
		- penting
		- [[Scicomm]]