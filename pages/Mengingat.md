-
  1. Undang-Undang RI Nomor 20 Tahun 2003 tentang Sistem Pendidikan Nasional;
-
  2. Undang-Undang RI Nomor 14 Tahun 2005 tentang Guru dan Dosen;
-
  3. Undang-Undang RI Nomor 12 Tahun 2012 tentang Pendidikan Tinggi;
-
  4. Undang-Undang RI Nomor 5 Tahun 2014 tentang Aparatur Sipil Negara;
-
  5. Peraturan Pemerintah RI Nomor 6 Tahun 1959 tentang Pendirian ITB;
-
  6. Peraturan Pemerintah RI Nomor 37 Tahun 2009 tentang Dosen;
-
  7. Peraturan Pemerintah RI Nomor 65 Tahun 2013 tentang Statuta Institut Teknologi Bandung;
-
  8. Peraturan Menteri Pendayagunaan Aparatur Negara dan Reformasi Birokrasi RI Nomor 17 Tahun 2013 tentang Jabatan Fungsional Dosen dan Angka Kreditnya;
-
  9. Peraturan Menteri Pendayagunaan Aparatur Negara dan Reformasi Birokrasi RI Nomor 46 Tahun 2013 tentang Perubahan atas Peraturan Menteri Pendayagunaan Aparatur Negara dan Reformasi Birokrasi RI Nomor 17 Tahun 2013 tentang Jabatan Fungsional Dosen dan Angka Kreditnya;
-
  10. Peraturan Menteri Pendidikan dan Kebudayaan RI Nomor 92 Tahun 2014 tentang Petunjuk Teknis Pelaksanaan Penilaian Angka Kredit Jabatan Fungsional Dosen;
-
  11. Peraturan Bersama Menteri Pendidikan dan Kebudayaan dan Kepala Badan Kepegawaian Negara RI Nomor 4/VIII/PB/2014 dan Nomor 24 Tahun 2014 tentang Ketentuan Pelaksanaan Peraturan Menteri Pendayagunaan Aparatur Negara dan Reformasi Birokrasi Nomor 17 Tahun 2013 sebagaimana telah diubah dengan Peraturan Menteri Pendayagunaan Aparatur Negara dan Reformasi Birokrasi Republik Indonesia Nomor 46 Tahun 2013 tentang Jabatan Fungsional Dosen dan Angka Kreditnya;
-
  12. Keputusan Majelis Wali Amanat ITB Nomor 001/SK/I1-MWA/KP/2019 tentang Pemberhentian Anggota Senat Akademik ITB Periode 2014-2019 dan Pengangkatan Anggota Senat Akademik ITB Periode 2019-2024;
-
  13. Keputusan Majelis Wali Amanat ITB Nomor 07/SK/I1-MWA/KP/2019 tentang Pemberhentian Ketua Senat Akademik ITB Periode 2014-2019 dan Pengangkatan Ketua Senat Akademik ITB Periode 2019-2024.
-
-
- Tambahan:
	- Peraturan Senat Akademik ITB [Nomor 15/SK/I1-SA/OT/2015](https://multisite.itb.ac.id/sa/wp-content/uploads/sites/44/2016/03/15_SK_I1-SA_OT_2015.pdf) tentang Proses
	  Penilaian dan Persetujuan Usulan Kenaikan Jabatan Dosen pada Jabatan
	  Lektor Kepala dan Profesor; dan
	- amandemennya Peraturan Senat Akademik ITB [Nomor 07/SK/I1-SA/OT/2016](https://multisite.itb.ac.id/sa/wp-content/uploads/sites/44/2016/03/07_SK_I1-SA_OT_2016.pdf)
	-