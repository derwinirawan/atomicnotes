- Dosen yang mengusulkan jabatan Lektor Kepala harus memenuhi kualifikasi dan kriteria:
	- 1) berijazah doktor dari program studi atau perguruan tinggi terakreditasi dalam bidang ilmu
	  yang sesuai dengan penugasan dan karya ilmiah/penelitiannya;
	- 2) mampu mendidik secara profesional;
	- 3)mampu menerapkan dan mengembangkan proses pembelajaran dan pembimbingan
	  secara mandiri bagi mahasiswa diploma, sarjana, dan/atau pascasarjana;
	- 4) mampu menganalisis bidang ilmu yang menjadi penugasannya;
	- 5) mampu menerapkan dan menganalisis teori bidang ilmu yang menjadi penugasannya dalam pelaksanaan penelitian dan pengabdian kepada masyarakat;
	- 6)memenuhi persyaratan khusus dan tambahan sebagaimana diatur dalam Pedoman Operasional Penilaian Angka Kredit Kenaikan Jabatan Akademik/Pangkat Dosen yang berlaku;
	-
	  > 7) selain untuk memenuhi persyaratan khusus pada butir 6) telah mempublikasikan hasil penelitiannya dalam jurnal internasional bereputasi sebagai penulis utama setelah kenaikan jabatan terakhir paling sedikit 1 buah; dan
	- 8) memiliki kinerja, integritas, tanggung jawab pelaksanaan tugas, etika, dan tata krama dalam kehidupan kampus.