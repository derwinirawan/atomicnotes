## Paparan
- ![[Pasted image 20210826155206.png]]
- ketinggian ombak tsunami 
	- tidak terlalu tinggi (3 m) - dibandingkan tsunami di daerah lain di dunia - tapi korban banyak -> daerah sumur
	- ketinggian ombak > 3 m korban tidak banyak
- total korban tsunami merak 450 m, bandingkan dengan mentawai yg 2000an
- bangunan 
	- terlalu dekat dengan pantai dan
	- tidak dibangun untuk menahan tsunami
- dampak tsunami terbatas di area selat sunda saja

## pesan 
- tantangan berat dalam memodelkan tsunami
- geomorfologi sangat mempengaruhi kekuatan gelombang tsunami
- penting untuk membangun sistem tsunami warning khususnya mute tsunami (non tectonics sources)
- data lebih banyak sangat dibutuhkan

## beberapa rujukan:
- https://nhess.copernicus.org/articles/3/321/2003/
- https://link.springer.com/article/10.1007%2Fs00024-020-02641-7
- https://link.springer.com/chapter/10.1007/1-4020-3331-1_4
- http://jrisetgeotam.lipi.go.id/index.php/jrisgeotam/article/view/301
- http://repo.kscnet.ru/1329/1/Paris_2014.pdf
- https://link.springer.com/article/10.1007%2Fs00024-020-02641-7
