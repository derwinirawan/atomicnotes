# Narsum 1
- Hendro Subagyo
- PDDI LIPI

## Latar belakang
- pentingnya berbagi data
- data sebagai komponen utama
- perubahan paradigma sains
	- transparansi
	- *reproducibility*
	- *reusability*

## Definisi
- `open` (terbuka)
	- bebas akses (free access 
	- bebas pakai
	- bebas berbagi
		
## Tujuan
- memicu kolaborasi
- memperluas partisipasi
- mempercepat pengembangan sains  
	
## Prinsip
- [FAIR]
	- Findable: dapat ditemukan
	- Accesible: dapat diakses
	- Interoperable: dapat dipertukarkan (via API)
	- Reusable: dapat digunakan ulang

- Prinsip lisensi dan etika
	- lisensi terbuka: Creative Commons
	- etika: data sensitif

## Protokol
- perencanaan
	- jenis data
	- ukuran data
	- jenis data
	- hak akses
- penyimpanan
	- FAIR
	- metadata penting: lengkap dan terbuka
	- koneksi dengan sistem lain
- berbagi data
	- format data
	- data sensitif
	- data non-sensitif
- preservasi data
	- *backup*

## Manfaat
- meningkatkan visibilitas peneliti
- meningkatkan jejaring
- bermanfaat bagi publik secara luas 
- sitasi data

## Terkait BRIN
- BRIN membuka berbagai sumber daya
	- [Repositori Ilmiah Nasional](https://rin.lipi.go.id)
	- [Mahameru LIPI HPC](https://hpc.lipi.go.id) (*high performance computing*) 
	- E-layanan sains (layanan laboratorium)
	- [Rujukan](https://rujukan.lipi.go.id) hosting jurnal akses terbuka
	- [RINarxiv preprint server](https://rinarxiv.lipi.go.id)
- Menciptakan *research hub* melalui berbagi data

# Narsum 2
- Mediareni Sulaiman
- PDDI LIPI

## Latar belakang
- #Ekuitas (*equity*) penting. Bukan hanya *#equality*.
- Semangat [demokratisasi pengetahuan](https://osf.io/fjm37/) (*democritizing knowledge*)
- [Artikel opini di Jakarta Post](https://www.thejakartapost.com/academia/2019/02/27/democratizing-knowledge-for-our-dream.html)

## Tujuan
- menyamakan persepsi sains terbuka dan pentingnya sains terbuka
- mempercepat pengembangan sains
- membangun kepercayaan (*trust*) kepada sains

## Isi dokumen
- definisi sains terbuka
- prinsip dasar: **Be [FAIR] and Be CARE**
- implementasi sains terbuka yang direkomendasikan
- infrastruktur (dan investasinya) sains terbuka yang direkomendasikan
- kerjasama antar pemangku kepentingan