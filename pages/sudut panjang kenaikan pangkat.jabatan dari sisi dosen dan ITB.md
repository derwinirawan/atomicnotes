title:: sudut panjang kenaikan pangkat/jabatan dari sisi dosen dan ITB

- sudut pandang dosen
  background-color:: #978626
	- Jabatan akademik adalah jabatan karir bagi dosen yang merupakan penghargaan atas prestasi akademik yang telah dicapai dan bentuk kepercayaan terhadap kemampuan akademik serta keteladanan dalam kehidupan akademik. Dosen berhak mendapatkan iklim akademik yang kondusif bagi pencapaian jabatan akademik maksimal. Dengan demikian dosen berkewajiban memanfaatkan fasilitas yang diberikan institusi untuk berprestasi sehingga memenuhi persyaratan jabatan akademik maksimal sesuai peraturan perundang undangan yang berlaku.
- Sudut pandang bagi ITB
  background-color:: #497d46
	- Jabatan akademik dosen merupakan komponen penting kualitas pendidikan tinggi. Dengan demikian ITB berhak mempunyai dosen dengan kualitas dan kuantitas yang memadai sesuai dengan standar kualitas yang mencerminkan ITB sebagai perguruan tinggi yang [[globally respected dan locally relevant]]. Oleh karena itu, ITB berkewajiban mengupayakan suasana kondusif dan mekanisme yang efektif agar proses kenaikan jabatan akademik/pangkat dosen dapat berjalan lancar.
	-
- [[Memutuskan]]